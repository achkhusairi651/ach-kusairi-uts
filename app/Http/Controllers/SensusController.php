<?php

namespace App\Http\Controllers;

use App\models\Sensus;
use Illuminate\Http\Request;

class SensusController extends Controller
{
    /**
     * Display a listing of the resource
     * 
     * @return \illuminate\http\Responce
     */
    public function index()
    {
        $allitem = Sensus::all();
        return view ('data',compact('allitem'));
    }

    /**
     * show the form for creating a new resource
     * 
     * @return \illuminate\http\Responce
     */
    public function create()
    {
        return view('buat');
    }

    /**
     * store a newly created resource in storage
     * 
     * @param \illuminate\http\Request  $request
     * @return \illuminate\http\Responce
     */
    public function store (Request $request)
    {
        Sensus :: create([
            'nama'=>$request->nama,
            'alamat'=>$request->alamat,
            'desa'=>$request->desa,

        ]);

        return redirect('data');
      
    }

    /**
     * Display the specified resource
     * 
     * @param \App\models\Sensus  $sensus
     * @return \illuminate\http\Responce
     */
    public function show (sensus $id)
    {
        $cari=sensus::find($id);
        return view ('cari', compact('cari'));
    }

    /**
     * show the form for editing the specified resource.
     * 
     * @param \App\Models\sensus  $sensus
     * @return \illuminate\Http\Responce
     */
    public function edit(sensus $sensus,$id)
    {
        $edit=  sensus::find($id);
        return view('edit',compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     * 
     * @param \illuminate\Http\Request  $request
     * @param \App\Models\Sensus  $sensus
     * @return \illuminate\Http\Responce
     */
    public function update(Request $request,Sensus $sensus,$id)
    {
        $edit= sensus::find($id);
        $edit->update($request->all());

        return redirect('data');
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param \App\models\sensus $sensus
     * @return \illuminate\Http\Responce
     */
    public function destroy(sensus $sensus,$id)
    {
        $edit = sensus::find($id);
        $edit ->delete();

        return back();    

    }
}