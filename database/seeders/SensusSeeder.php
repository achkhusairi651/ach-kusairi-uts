<?php

namespace Database\Seeders;

use App\Models\Sensus;
use Illuminate\Database\Seeder;

class SensusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sensus::create([
            'nama'=>'Achkusairi',
            'alamat'=>'mapper',
            'desa'=>'mapper',
        ]);
    }
}
