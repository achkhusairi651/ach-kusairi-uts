<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SensusController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('data',[SensusController::class,'index']);
Route::get('data/{id}',[SensusController::class,'show']);
Route::get('buat',[SensusController::class,'create']);
Route::Post('simpan',[SensusController::class,'store']);
Route::get('edit/{id}',[SensusController::class,'edit']);
Route::post('update/{id}',[SensusController::class,'update']);
Route::get('delete/{id}',[SensusController::class,'destroy']);