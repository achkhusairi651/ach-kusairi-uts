<form action="{{ url('update', $edit->id) }}" method="post">
    {{ csrf_field() }}
    <table border="5px" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <tr>
            <th>Nama :</th>
            <td>
                <input type="text" name="nama" id="nama" value="{{ $edit->nama }}">
            </td>
        </tr>
        <tr>
            <th>alamat :</th>
            <td>
                <input type="text" name="alamat" id="alamat" value="{{ $edit->alamat }}">
            </td>
        </tr>
        <tr>
            <th>desa :</th>
            <td>
                <input type="text" name="desa" id="desa" value="{{ $edit->desa }}">
            </td>
        </tr>
        <tr>
            <td>
                <a href="/data">Kembali</a>
            </td>
            <td>
                <button>EDIT</button>
            </td>
        </tr>
    </table>
</form>